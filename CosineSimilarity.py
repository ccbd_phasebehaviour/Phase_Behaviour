
# coding: utf-8

# In[1]:


import numpy as np




def cos_sim(a, b):
    """Takes 2 vectors a, b and returns the cosine similarity according 
    to the definition of the dot product
    """
    dot_product = np.dot(a,b)
    norm_a = np.linalg.norm(a)
    norm_b = np.linalg.norm(b)
    return dot_product / (norm_a * norm_b)



#x1 = np.array([1,2,3])
#x2 = np.array([4,5,6])


#print(cos_sim(x1,x2))


# In[2]:


import numpy as np
import pandas as pd



l=[]
df = pd.read_csv("Downloads/ccbdstuff/grep.csv",header=None)
x=np.array(df)
for i in range(1,len(x)):
    x1=x[i-1] 
    x2=x[i]
    c=cos_sim(x1,x2)
    l.append(c)
    
print(l)    
    
    


# In[4]:


import numpy as np
import pandas as pd



q=[]
df1 = pd.read_csv("Downloads/ccbdstuff/ls.csv",header=None)
x=np.array(df1)
for i in range(1,len(x)):
    x1=x[i-1] 
    x2=x[i]
    c=cos_sim(x1,x2)
    q.append(c)
    
print(q)    


# In[5]:


import numpy as np
import pandas as pd



p=[]
df2 = pd.read_csv("Downloads/ccbdstuff/gcc.csv",header=None)
x=np.array(df2)
for i in range(1,len(x)):
    x1=x[i-1] 
    x2=x[i]
    c=cos_sim(x1,x2)
    p.append(c)
    
print(p)    


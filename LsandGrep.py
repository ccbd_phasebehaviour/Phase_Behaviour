
# coding: utf-8

# In[1]:


import numpy as np




def cos_sim(a, b):
    """Takes 2 vectors a, b and returns the cosine similarity according 
    to the definition of the dot product
    """
    dot_product = np.dot(a,b)
    norm_a = np.linalg.norm(a)
    norm_b = np.linalg.norm(b)
    return dot_product / (norm_a * norm_b)



#x1 = np.array([1,2,3])
#x2 = np.array([4,5,6])


#print(cos_sim(x1,x2))



# In[2]:


import numpy as np
import pandas as pd



l=[]
df = pd.read_csv("Downloads/ccbdstuff/combinedlsgrep.csv",header=None)
x=np.array(df)
for i in range(1,len(x),2):
    x1=x[i-1] 
    x2=x[i]
    c=cos_sim(x1,x2)
    l.append(c)
    
print(l)    

#considering threshold as 0.9995, we see that there are 6 entries below the threshold that means many vectors are dissimilar 
#by cosine similarity
    


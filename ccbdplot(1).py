
# coding: utf-8

# In[2]:


#Different number of execution phases for each application
#Considering a common threshold value of 0.99998
import matplotlib.pyplot as plt

import plotly.plotly as py


y = [6,0,1]
N = len(y)
x = ['ls','gcc','grep']
width = 1/1.5
plt.bar(x, y, width, color="blue")


# In[3]:


#for grep 
import matplotlib.pyplot as plt




y = [0.9999805451114726, 0.9999986501202811, 0.9999981526386296, 0.9999996993586007, 0.9999895847407292, 0.9999883532751148, 0.999990685997778, 0.9999573153636387, 0.9999889590047574]
N = len(y)
x = range(N)
width = 1/1.5
plt.bar(x, y, width, color="blue")


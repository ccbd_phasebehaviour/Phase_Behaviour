
# coding: utf-8

# In[1]:


from sklearn.cluster import KMeans
import numpy as np
import pandas as pd
df = pd.read_csv("Downloads/ccbdstuff/kmeans_entries.csv",header=None)
a=np.array(df)
kmeans = KMeans(n_clusters=3, random_state=0).fit(a)
print(kmeans.labels_)

print(kmeans.predict([[1272692,83671,24496,11547],[1547275,63839,20232,12725]]))

print(kmeans.cluster_centers_)

